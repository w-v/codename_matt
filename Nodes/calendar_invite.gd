extends Control

var meetings = {}
var meeting_nodes = []
var frame_count = 0
var columns = []
var time = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	if meetings.size() == 0:
		ServerController.all_player_data = MeetingsController.DEBUG_PLAYER_DATA
		for host in MeetingsController.DEBUG_PLAYER_DATA:
			print("MEETINGS FOR PLAYER %s" % host)
			meetings[host] = []
			for i in range(0, 3):
				var meeting = MeetingsController.generate_meeting(host)
				meeting.start_time = i * 2
				print("MeetingID  %s" % meeting.id)
				print("description  %s" % meeting.description)
				print("length  %s" % meeting.length)
				print("participants")
				for p in meeting.participants:
					print("  %s" % p)
				meetings[host].append(meeting)
	add_invites_slots()
	get_parent().find_child("advance_button").pressed.connect(reveal_meetings)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if frame_count == 3:
		add_meetings()
	frame_count += 1


func add_meetings():
	var current_player_id = multiplayer.get_unique_id()
	var player_data = ServerController.all_player_data
	var meeting_scene = load("res://Nodes/meeting.tscn")
	var player_index = 0
	for playerID in player_data:
		for meeting in meetings[playerID]:
			# only display meetings you're in 
			# if current_player_id not in meeting.participants:
			# 	continue
			var meeting_node = meeting_scene.instantiate()
			meeting_node.dragging_enabled = false
			meeting_node.meeting = meeting
			meeting_node.visible = false
			# meeting_node.configure()
			columns[player_index].add_meeting(meeting_node)
			meeting_nodes.append(meeting_node)
		player_index += 1

func add_invites_slots():
	var player_data = ServerController.all_player_data
	for playerID in player_data:
		var player_calendar_column = load("res://Nodes/player_calendar_column.tscn").instantiate()
		var player_label = player_calendar_column.find_child("Player_label")
		var column = player_calendar_column.find_child("calendar_column_control")
		var header = column.find_child("header")
		column.remove_child(header)
		player_label.configure(player_data[playerID])
		player_label.set_status(Globals.COLOUR_RED)
		add_child(player_calendar_column)
		columns.append(column)

func reveal_meetings():
	for meeting_node in meeting_nodes:
		if meeting_node.meeting.start_time <= time:
			meeting_node.visible = true
		else:
			meeting_node.visible = false
	
	time += 1



		
		
