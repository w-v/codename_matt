extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	add_hours()
	

func add_hours():
	var hour_labels_container = find_child("hour_labels_container")
	var hour_label_scene = load("res://Nodes/hour_label.tscn")
	for hour in range(Globals.HOURS_RANGE[0], Globals.HOURS_RANGE[1] + 1):
		var hour_label_element = hour_label_scene.instantiate()
		hour_label_element.find_child("hour_label").text = "%dh" % hour
		hour_labels_container.add_child(hour_label_element)


# func get_slots():
# 	return find_children("meeting_slot")

