extends Node

const MAX_PLAYERS = 8
const MIN_PLAYERS = 1 #TODO 3
const TIME_TO_SCHEDULE = 300

const HOURS_RANGE = [9, 17]

var COLOUR_GREEN = Color(0.15, 0.71, 0.15)
var COLOUR_YELLOW = Color(0.98, 0.93, 0.12)
var COLOUR_RED = Color(0.96, 0.15, 0.11)
