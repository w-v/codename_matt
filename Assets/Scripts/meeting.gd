extends Control

var meeting
var meeting_preview
var dragging_enabled = true


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func configure():
	$TextureRect/Description.text = meeting.description
	if meeting.length == 1:
		$TextureRect.texture = load("res://Assets/Images/Calendar/meeting_1h.png")
	elif meeting.length == 2:
		$TextureRect.texture = load("res://Assets/Images/Calendar/meeting_2h.png")
	else:
		$TextureRect.texture = load("res://Assets/Images/Calendar/meeting_3h.png")
	var participants_string = ""
	var invitee_prefab = load("res://Nodes/player_icon.tscn")
	# remove existing invitees before making them
	for invitee in $TextureRect/Invitees.get_children():
		$TextureRect/Invitees.remove_child(invitee)
	for p in meeting.participants:
		participants_string += str(p) + ","
		var participant = ServerController.all_player_data[p]
		var invitee = invitee_prefab.instantiate()
		invitee.get_node("Background").modulate = participant[PlayerData.FIELDS.COLOUR]
		invitee.get_node("Name").text = PlayerData.get_initial(participant[PlayerData.FIELDS.NAME])
		$TextureRect/Invitees.add_child(invitee)

func _notification(what):
	if what == NOTIFICATION_DRAG_END and not get_viewport().gui_is_drag_successful():
		visible = true


func _get_drag_data(at_position):
	# var meeting_preview = load("res://Nodes/meeting.tscn").instantiate()
	# meeting_preview.meeting = meeting
	# meeting_preview.configure()
	if not dragging_enabled:
		return null
	meeting_preview = self.duplicate()
	meeting_preview.z_index = 3
	set_drag_preview(meeting_preview)
	remove_child(meeting_preview)
	get_parent().add_child(meeting_preview)
	# meeting_preview.position = -0.5 * meeting_preview.size
	visible = false
	return self
