extends Control

var frame_count = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	#ServerController.all_player_data = MeetingsController.DEBUG_PLAYER_DATA
	# var meeting_node = load("res://Nodes/meeting.tscn").instantiate()
	# add_child(meeting_node)
	# meeting_node.meeting = MeetingsController.generate_meeting(0)

	# meeting_node = load("res://Nodes/meeting.tscn").instantiate()
	# add_child(meeting_node)
	# meeting_node.meeting = MeetingsController.generate_meeting(0)

func generate_meetings():
	#ServerController.all_player_data = MeetingsController.DEBUG_PLAYER_DATA

	var calendar_node = find_child("scheduling_calendar")
	var calendar_column_node = calendar_node.find_child("calendar_column_container")
	var slot_size = calendar_column_node.slot_size
	var slot_width = calendar_column_node.size.x
	print(slot_width)

	var meeting_count
	var player_count = len(ServerController.all_player_data)
	if player_count == 3:
		meeting_count = 3
	else:
		meeting_count = 2
	
	var player_id = multiplayer.get_unique_id()
	var meeting_container = find_child("scheduling_meetings")
	var generated_meeting
	var meeting_scene = load("res://Nodes/meeting.tscn")
	var last_meeting_bottom = 0
	for meeting_index in range(meeting_count):
		generated_meeting = meeting_scene.instantiate()
		generated_meeting.meeting = MeetingsController.generate_meeting(player_id)
		generated_meeting.configure()
		generated_meeting.size.y = slot_size * generated_meeting.meeting.length
		generated_meeting.size.x = slot_width
		meeting_container.add_child(generated_meeting)
		generated_meeting.position.y = last_meeting_bottom + 50
		last_meeting_bottom = generated_meeting.position.y + generated_meeting.size.y

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# if frame_count < 3:
	# 	frame_count += 1
	if frame_count == 1:
		generate_meetings()


	frame_count += 1


func get_user_meetings():
	var calendar_node = find_child("scheduling_calendar")
	var calendar_column_node = calendar_node.find_child("calendar_column_container")
	var user_meetings = []
	for meeting_node in calendar_column_node.meetings:
		user_meetings.append(meeting_node.meeting)

	return user_meetings

func _on_ready_pressed():
	PlayerData.is_ready = !PlayerData.is_ready
	if PlayerData.is_ready:
		$Ready/Label.text = "Unready"
	else:
		$Ready/Label.text = "Ready"
