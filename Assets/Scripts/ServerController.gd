extends Node

signal on_changed

# Map of {int playerid => PlayerData data}
var all_player_data = {}:
	set(value):
		all_player_data = value
		on_changed.emit()

func _ready():
	multiplayer.connected_to_server.connect(register_me_on_server)

func configure_server():
	multiplayer.peer_disconnected.connect(_on_player_disconnected)
	on_changed.connect(send_data_to_clients)
	register_me_on_server()

func check_all_ready():
	for player in all_player_data:
		if not all_player_data[player][PlayerData.FIELDS.READY]:
			return false
	return true

func _on_player_disconnected(id):
	if not multiplayer.is_server():
		return
	# Remove player from all_player_data
	all_player_data.erase(id)
	send_data_to_clients()

func register_me_on_server():
	# Send my info to the host
	send_data_to_server(PlayerData.package_data())
	PlayerData.on_changed.connect(send_data_to_server)

# Send a single player's data to the server, which propagates to all connected players
func send_data_to_server(player_data):
	rpc_send_data_to_server.rpc_id(1, player_data)

@rpc("any_peer", "reliable", "call_local")
func rpc_send_data_to_server(player_data):
	if not multiplayer.is_server():
		return
	all_player_data[multiplayer.get_remote_sender_id()] = player_data
	send_data_to_clients()

# Update all players with all player data
func send_data_to_clients():
	rpc_send_data_to_clients.rpc(all_player_data)

@rpc("authority", "reliable", "call_remote")
func rpc_send_data_to_clients(player_data):
	all_player_data = player_data
