extends Control

var column
var n_slots
var slot_size
var meetings = []

var first_run = true

# Called when the node enters the scene tree for the first time.
func _ready():
	column = find_child("calendar_column")
	n_slots = Globals.HOURS_RANGE[1] - Globals.HOURS_RANGE[0]
	#ServerController.all_player_data = MeetingsController.DEBUG_PLAYER_DATA
	add_slots()

func _process(delta):
	if first_run:
		slot_size = (column.size.y / n_slots)
		first_run = false
	# if not first_run:
	# 	var meeting_obj = MeetingsController.generate_meeting(0)
	# 	meeting_obj.start_time = 0
	# 	var meeting_node = load("res://Nodes/meeting.tscn").instantiate()
	# 	meeting_node.meeting = meeting_obj
	# 	add_meeting(meeting_node)

	# 	meeting_obj = MeetingsController.generate_meeting(0)
	# 	meeting_obj.start_time = 0
	# 	meeting_node = load("res://Nodes/meeting.tscn").instantiate()
	# 	meeting_node.meeting = meeting_obj
	# 	add_meeting(meeting_node)

	# 	first_run = true

func add_slots():
	var slot_container_node = find_child("slot_container")
	var slot_scene = load("res://Nodes/calendar_slot.tscn")
	var slot_node
	for slot_index in range(n_slots):
		slot_node = slot_scene.instantiate()
		slot_container_node.add_child(slot_node)

func add_meeting(meeting_node):
	meeting_node.configure()
	column.add_child(meeting_node)
	meeting_node.position.y = (column.size.y / n_slots) * meeting_node.meeting.start_time
	meeting_node.size.x = column.size.x
	meeting_node.size.y = (column.size.y / n_slots) * meeting_node.meeting.length
	meetings.append(meeting_node)

func _can_drop_data(at_position, meeting_node):
	var slot_index_start = floor(at_position.y / slot_size)
	var slot_index_end = slot_index_start + meeting_node.meeting.length - 1
	if slot_index_end >= n_slots:
		return false
	var meeting_start
	var meeting_end
	for other_meeting_node in meetings:
		if other_meeting_node.meeting.id == meeting_node.meeting.id:
			continue
		meeting_start = other_meeting_node.meeting.start_time
		meeting_end = other_meeting_node.meeting.start_time + other_meeting_node.meeting.length - 1
		if (
			(
				slot_index_start >= meeting_start
				and slot_index_start <= meeting_end
			)
			or (
				slot_index_end >= meeting_start
				and slot_index_end <= meeting_end
			)
			or (
				meeting_start >= slot_index_start
				and meeting_end <= slot_index_end
			)
		):
			return false

	var preview_node = meeting_node.meeting_preview
	preview_node.global_position.y = column.global_position.y + slot_size * slot_index_start
	preview_node.global_position.x = column.global_position.x
	# preview_node_container.size.x = column.size.x
	# preview_node_container.size.y = slot_size * meeting_node.meeting.length
	return true
	
func _drop_data(at_position, meeting_node):
	var slot_index = floor(at_position.y / slot_size)
	meeting_node.meeting.start_time = slot_index
	meeting_node.get_parent().remove_child(meeting_node)
	meeting_node.visible = true
	add_meeting(meeting_node)
