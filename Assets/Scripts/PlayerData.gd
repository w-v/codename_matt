extends Node

signal on_changed

enum FIELDS {NAME,READY,COLOUR}

var colour_options = [
	Color(.88,.79,.28),  # yellow
	Color(.4,.74,.23),  # green
	Color(.2,.68,.52),  # teal
	Color(.27,.27,.76),  # blue
	Color(.52,.23,.71),  # purple
	Color(.82,.27,.74),  # fushia
	Color(.83,.27,.45),  # magenta
	Color(.91,.26,.26)  # red
]

var player_name = "":
	set(value):
		if value != player_name:
			player_name = value
			on_changed.emit(package_data())

var is_ready = false:
	set(value):
		if value != is_ready:
			is_ready = value
			on_changed.emit(package_data())

var colour = colour_options.pick_random():
	set(value):
		if value != colour:
			colour = value
			on_changed.emit(package_data())

func package_data():
	return {
		FIELDS.NAME: player_name,
		FIELDS.READY: is_ready,
		FIELDS.COLOUR: colour
	}

func get_initial(_player_name):
	var initials = _player_name[0]
	if _player_name.length() > 1:
		initials += _player_name[1]
	return initials
