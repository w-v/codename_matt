extends Node

var next_meeting_id = 0
const MAX_MEETING_LENGTH = 3

var all_meeting_descriptions = [
	"Look at cat pictures together",
	"Think about working",
	"Gossip with coworkers",
	"Write Important Lists",
	"Take a coffee break",
	"Sort paperclips ",
	"Write each other Post-it notes",
	"Fix the printer",
	"Complain about the printer",
	"Commit HR violations",
	"Day drink with coworkers",
	"This could have been an email",
	"Ignore deadlines together",
	"Gaslight QA",
	"Watch football",
	"Make out-of-context memes together",
]

var remaining_meeting_descriptions = all_meeting_descriptions.duplicate()

var DEBUG_PLAYER_DATA = {
	1: {
		PlayerData.FIELDS.NAME: '0estHOST',
		PlayerData.FIELDS.READY: false,
		PlayerData.FIELDS.COLOUR: PlayerData.colour_options.pick_random()
	},
	12345: {
		PlayerData.FIELDS.NAME: '1est1',
		PlayerData.FIELDS.READY: false,
		PlayerData.FIELDS.COLOUR: PlayerData.colour_options.pick_random()
	},
	92311: {
		PlayerData.FIELDS.NAME: '2est2',
		PlayerData.FIELDS.READY: false,
		PlayerData.FIELDS.COLOUR: PlayerData.colour_options.pick_random()
	},
	571421: {
		PlayerData.FIELDS.NAME: '3est3',
		PlayerData.FIELDS.READY: false,
		PlayerData.FIELDS.COLOUR: PlayerData.colour_options.pick_random()
	},
	2011: {
		PlayerData.FIELDS.NAME: '4est4',
		PlayerData.FIELDS.READY: false,
		PlayerData.FIELDS.COLOUR: PlayerData.colour_options.pick_random()
	},
}

class Meeting:
	var id: int
	var description: String
	var length: int
	var participants: Array
	var host: int
	var start_time: int

func generate_meeting(host):
	var meeting = Meeting.new()
	meeting.id = next_meeting_id
	next_meeting_id += 1
	meeting.description = _get_meeting_description()
	meeting.length = randi() % MAX_MEETING_LENGTH + 1
	meeting.participants = _get_meeting_participants(host)
	meeting.host = host
	meeting.start_time = -1
	return meeting
	
func _get_meeting_description():
	var meeting_index = randi() % remaining_meeting_descriptions.size()
	var selected_description = remaining_meeting_descriptions[meeting_index]
	remaining_meeting_descriptions.remove_at(meeting_index)
	if remaining_meeting_descriptions.size() == 0:
		remaining_meeting_descriptions = all_meeting_descriptions.duplicate()
	return selected_description

func _get_meeting_participants(host):
	var players = []
	var meeting_participants = []
	for player_id in ServerController.all_player_data:
		if player_id != host:
			players.append(player_id)
	while true:
		var player_index = randi() % players.size()
		if meeting_participants.find(players[player_index]) != -1:
			break
		meeting_participants.append(players[player_index])
	return meeting_participants

func _ready():
	if true:
		return
	# TEST OUTPUT
	ServerController.all_player_data = DEBUG_PLAYER_DATA
	for host in DEBUG_PLAYER_DATA:
		print("MEETINGS FOR PLAYER %s" % host)
		for i in range(0, 3):
			var meeting = generate_meeting(host)
			print("MeetingID  %s" % meeting.id)
			print("description  %s" % meeting.description)
			print("length  %s" % meeting.length)
			print("participants")
			for p in meeting.participants:
				print("  %s" % p)
			print()
	ServerController.all_player_data = {}
